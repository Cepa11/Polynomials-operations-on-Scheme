#lang racket
; Authors:
; _Marco Alfaro Ramirez 
; _Jose Ramirez Gomez 
; _Cristian Nunez Tapia 
; Paradigmas de Programación
; Ciclo II 2018
; ====================================================
(define getXGrade
  (lambda (_grade)
    (cond
      ((> _grade 1)
       (string-append "x^" (~a _grade)))
      ((= _grade 1)
       "x")
      (else
       ""))))

(define appendXWithGrade
  (lambda (_number _grade)
    (cond
      ((= _grade 0)
       (string-append (~a _number) (getXGrade _grade)))
      ((and (= _number 1) (>= _grade 1))
       (string-append " + "(getXGrade _grade)))
      ((and (= _number -1) (>= _grade 1))
       (string-append " - "(getXGrade _grade)))
      ((and (> _number 0) (> _grade 1))
       (string-append " + " (string-append (~a _number) (getXGrade _grade))))
      ((< _number 0)
       (string-append " - "(string-append (~a (* _number -1)) (getXGrade _grade))))
      (else
       ""))))

(define traverseList
  (lambda (_list _counter _result)
    ;(printf "_list:~a _counter:~a _result:~a" _list _counter _result)(newline)
    (cond
      ((null? _list)
       _result)
      (else
       (traverseList (cdr _list) (+ _counter 1) (string-append _result (appendXWithGrade (car _list) _counter)))))))

(define pass
  (lambda (_list _result)
    (cond
      ((null? _list)
       _result)
      (else
        (pass (cdr _list) (string-append _result (traverseList (car _list) 0 "")))))))
        


(define display-p
  (lambda _list
    (pass _list "")))

(provide (all-defined-out))