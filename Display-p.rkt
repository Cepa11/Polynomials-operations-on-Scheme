#lang racket
; Authors:
; _Marco Alfaro Ramirez 
; _Jose Ramirez Gomez 
; _Cristian Nunez Tapia 
; Paradigmas de Programación
; Ciclo II 2018
; ====================================================
(define display-p
  (lambda (L)
    (d2 L "" 0)))

(define d2
  (lambda (L result count)
    (if (null? L) result
    (cond ((and (= count 0) (not (= (car L) 0)) ) (d2 (cdr L) (string-append result (number->string (car L))" + ") (+ 1 count)))
          ((= (car L) 0) (d2 (cdr L) result (+ 1 count)))
          ((and (= count 1) (= (car L) 1)) (cond ((< 0 (car L))(d2 (cdr L) (string-append result "x ") (+ 1 count)))((> 0 (car L))(d2 (cdr L) (string-append result "-x ") (+ 1 count)))))
          ((= count 1) (d2 (cdr L) (string-append result (number->string (car L)) "x ") (+ 1 count)))
          (else
          (d2 (cdr L) (string-append result (if (equal? result "") " " "+ ") (if (= (car L) 1)""(number->string (car L))) "x^" (number->string count)) (+ 1 count)))))))
(provide (all-defined-out))